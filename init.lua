fissures = {}

local pr
local function nextrandom(min, max)
	return pr:next() / 32767 * (max - min) + min
end

local function sq(a)
	return a*a
end

-- returns the squared plane (=without y) distance between p1 and p2
local function sq_planedist(p1, p2)
	return sq(p2.x - p1.x) + sq(p2.z - p1.z)
end

-- Returns if pos is in the fissure. Expects a fissure like returned from fissures.longhighcave()
function fissures.is_in_fissure(pos, fissure)
	for i = 1, #fissure do
		local waypoint = fissure[i]
		if (pos.y >= waypoint.y) and (pos.y <= waypoint.y + waypoint.height) then
			local thickness = waypoint.thickness * waypoint.thickness
			
			-- special heights: roof, 2st "floor"
			if waypoint.y + waypoint.height - pos.y < 5 then
				thickness = thickness * (waypoint.y + waypoint.height - pos.y) / 5
			elseif pos.y == math.floor(waypoint.y + waypoint.height / 2) then
				thickness = thickness - 1.5
			end

			-- is pos in a segment?
			if sq_planedist(pos, waypoint) < thickness then
				return true
			end
		end
	end
	return false
end

-- Returns min, max boundaries of a fissure object
function fissures.get_boundaries(fissure)
	local min, max
	for i = 1, #fissure do
		-- Boundaries of local segment
		local segment = fissure[i]
		local segment_min = {
			x=segment.x - sq(segment.thickness),
			y=segment.y,
			z=segment.z - sq(segment.thickness),
		}
		local segment_max = {
			x=segment.x + sq(segment.thickness),
			y=segment.y + segment.height,
			z=segment.z + sq(segment.thickness),
		}

		-- If necessary, update fissure boundary
		if min == nil then
			min = segment_min
		else
			min.x = math.min(min.x, segment_min.x)
			min.y = math.min(min.y, segment_min.y)
			min.z = math.min(min.z, segment_min.z)
		end

		if max == nil then
			max = segment_max
		else
			max.x = math.max(max.x, segment_max.x)
			max.y = math.max(max.y, segment_max.y)
			max.z = math.max(max.z, segment_max.z)
		end
	end
	return min, max
end

-- Probability for every newly generated chunk to get fissures
local probability_fissures_in_chunk = 2/5

-- Returns a fissure, a data structure containing cave segments.
function fissures.longhighcave(starting_point)
	local phi = nextrandom(0, 90) --3.141592)
	local height = nextrandom(10,nextrandom(12, 50))
	local thickness = nextrandom(2, 4.2)
	local segment_length = 2
	local length = nextrandom(14, nextrandom(42, 100))/segment_length

	local x,z,y = starting_point.x, starting_point.y, starting_point.z
	local minx, minz, miny = x - thickness, z - thickness, y
	local maxx, maxz, maxy = x + thickness, z + thickness, y + height
	
	-- fissure: An array of points + thickness + cave height
	local fissure = {[1]={x=x, y=y, z=z, height=height, thickness=thickness}, length=length}
	for i = 2, length do
		-- Add new segment to the cave
		x = x + math.cos(phi) * segment_length
		z = z + math.sin(phi) * segment_length
		y = y + nextrandom(-0.1,0.1)
		height = height + nextrandom(-0.5,0.5)
		thickness = thickness + nextrandom(-0.5,0.5)
		phi = phi + nextrandom(-0.1,0.1)
		fissure[i] = {x=x, y=y, z=z, height=height, thickness=thickness}

		-- Min & Max
		minx, minz, miny = math.min(minx, x - thickness), math.min(minz, z - thickness), math.min(miny, y)
		maxx, maxz, maxy = math.max(maxx, x + thickness), math.max(maxz, z + thickness), math.max(maxy, y + height)
	end
	fissure.min = {x=minx, y=miny, z=minz}
	fissure.max = {x=maxx, y=maxy, z=maxz}
	return fissure
end

-- Renders a fissure to the map, using a voxel manipulator
function fissures.render_longhighcave(fissure)
	local min, max = fissures.get_boundaries(fissure)
	minetest.emerge_area(min, max)
	local vm = VoxelManip(min, max)
	local emin, emax = vm:get_emerged_area()
	local area = VoxelArea:new{MinEdge=emin, MaxEdge=emax}
	local data = vm:get_data()

	-- For every voxel part of the fissure, put air in it
	for vi in area:iterp(emin, emax) do
		local pos = area:position(vi)
		if fissures.is_in_fissure(pos, fissure) then
			data[vi] = minetest.CONTENT_AIR
		end
	end
	vm:set_data(data)
	vm:write_to_map(true)
end

-- This function will be registered for being called on every generated map chunk
function fissures.chunk_func(minp, maxp, seed)
	if not pr then
		pr = PseudoRandom(seed)
	end

	for i = 1,5 do
		local startpos = vector.round({x=pr:next(minp.x, maxp.x), y=pr:next(minp.y, maxp.y), z=pr:next(minp.z, maxp.z)})
		local fissure = fissures.longhighcave(startpos)
		fissures.render_longhighcave(fissure)
	end
end

minetest.register_on_generated(fissures.chunk_func)
print("[fissures] Registered worldgen function")
